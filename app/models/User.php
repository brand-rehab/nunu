<?php
declare(strict_types=1);

namespace NuNu\App\Models;

use BrandRehab\PimPush\Entity\Model;

class User extends Model
{
  protected $table = 'user';

  protected $assigned = [
    'firstname',
  ];

  protected $encrypted = [

  ];

  protected $hashed = [

  ];

  protected $revisioned = [

  ];
}
