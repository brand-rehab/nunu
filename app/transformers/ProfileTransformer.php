<?php
declare(strict_types=1);

namespace NuNu\App\Transformers;

use \stdClass;
use NuNu\App\Models\User;
use BrandRehab\PimPush\Entity\Transformer;

class ProfileTransformer extends Transformer
{
  public function __construct()
  {
    $this->model = new User();
  }

  protected function model(stdClass $model): array
  {
    return [
      'id' => $model->id,
      'name' => $model->firstname.' '.$model->lastname,
    ];
  }
}
