<?php
declare(strict_types=1);

namespace NuNu\App\Controllers;

use \stdClass;
use NuNu\App\Models\User;
use NuNu\App\Transformers\MemberTransformer;
use BrandRehab\PimPush\Http\Controller;

class MemberController extends Controller
{
  public function read(string $id): void
  {
    $user = new User();
    $user->firstOrFail($id); // this would need a new mwthod called firstOr404 which returns an error page

    $transformer = new MemberTransformer();
    $this->response->twig(200, 'member', $transformer->transform($user));
  }
}
