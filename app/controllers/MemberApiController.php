<?php
declare(strict_types=1);

namespace NuNu\App\Controllers;

use \stdClass;
use NuNu\App\Models\User;
use NuNu\App\Transformers\MemberTransformer;
use BrandRehab\PimPush\Http\Controller;

class MemberApiController extends Controller
{
  public function read(string $id): void
  {
    $user = new User();
    $user->firstOrFail($id);

    $transformer = new MemberTransformer();
    $this->response->json(200, $transformer->transform($user));
  }
}
