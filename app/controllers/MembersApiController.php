<?php
declare(strict_types=1);

namespace NuNu\App\Controllers;

use \stdClass;
use NuNu\App\Models\User;
use NuNu\App\Transformers\MemberTransformer;
use BrandRehab\PimPush\Http\Controller;

class MembersApiController extends Controller
{
  public function create(stdClass $payload): void
  {
    $user = new User();
    $id = $user->create($payload)->save();
    $this->response->header(201, ['location' => "/member/{$id}"]);
  }

  public function read(): void
  {
    $users = new User();
    $users->allOrFail();

    $transformer = new MemberTransformer();
    $this->response->json(200, $transformer->transform($users));
  }
}
