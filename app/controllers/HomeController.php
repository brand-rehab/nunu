<?php
declare(strict_types=1);

namespace NuNu\App\Controllers;

use BrandRehab\PimPush\Http\Controller;

class HomeController extends Controller
{
  public function read(): void
  {
    $this->response->twig(200, 'home');
  }
}
