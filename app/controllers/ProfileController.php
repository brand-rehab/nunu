<?php
declare(strict_types=1);

namespace NuNu\App\Controllers;

use \stdClass;
use NuNu\App\Models\User;
use NuNu\App\Transformers\ProfileTransformer;
use BrandRehab\PimPush\Http\Controller;

class ProfileController extends Controller
{
  public function read(): void
  {
    $user = new User();
    $user->firstOrFail(1);

    $transformer = new ProfileTransformer();
    $this->response->twig(200, 'profile', $transformer->transform($user));
  }

  public function append(stdClass $payload): void
  {
    /**
     * @todo define method
     */
     var_dump('turned out nice again');
     die();
  }

  public function purge(string $id): void
  {
    /**
     * @todo define method
     */
  }
}
