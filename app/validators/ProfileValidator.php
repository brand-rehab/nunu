<?php
declare(strict_types=1);

namespace NuNu\App\Validators;

use NuNu\App\Models\User;

class ProfileValidator
{
  private $model;

  public __construct(array $model)
  {
    $this->model = $model;
  }
}
