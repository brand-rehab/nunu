<?php
declare(strict_types=1);

return [
  "database" => [
    "host" => "localhost",
    "name" => "",
    "user" => "",
    "password" => "",
  ],
  "server" => [
    "environment" => "development",
    "base_uri" => "http://example.domain/"
  ],
  "middleware" => [
    "cors" => false,
  ]
];
