<?php
declare(strict_types=1);

require "vendor/autoload.php";

use BrandRehab\PimPush\App\NuNu;

$app = new NuNu();
$app->run();
