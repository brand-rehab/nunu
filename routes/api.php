<?php

include('web.php');

$this->route()->get('/api/profile', 'ProfileApiController');
$this->route()->get('/api/members', 'MembersApi/Controller');
$this->route()->post('/api/members', 'MembersApi/Controller');
$this->route()->get('/api/member/{id}', 'MemberApiController', 'id');
