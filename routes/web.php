<?php

$this->route()->get('/', 'HomeController');
$this->route()->get('/profile', 'ProfileController');
$this->route()->get('/members', 'MembersController');
$this->route()->post('/members', 'MembersController');
$this->route()->get('/member/{id}', 'MemberController', 'id');
