<?php
declare(strict_types=1);

namespace NuNu\Database\Schemas;

class UserSchema
{
  protected $table_name = 'user';

  protected $columns = [
    'id' => [
      'type' => 'primary'
    ],
    'firstname' => [
      'type' => 'varchar',
      'length' => 100,
    ],
    'lastname' => [
      'type' => 'varchar',
      'length' => 100,
    ],
  ];

  public function getColumns(): array
  {
    return $this->columns;
  }

  public function getTableName(): string
  {
    return $this->table_name;
  }
}
