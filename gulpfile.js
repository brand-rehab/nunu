// includes

var gulp = require('gulp');
var babel = require('gulp-babel');
var concat = require('gulp-concat');
var autoprefixer = require('autoprefixer');
var mqpacker = require('css-mqpacker');
var postcss = require('gulp-postcss');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var hash = require('gulp-hash');
var clean = require('gulp-clean');

//variables

var _sass = [
  'resources/src/sass/bootstrap.scss',
  'resources/src/sass/simple-line-icons.scss',
  'resources/src/sass/app.scss'
];

var _js = [
  'node_modules/popper.js/dist/umd/popper.js',
  'node_modules/jquery/dist/jquery.js',
  'node_modules/bootstrap/dist/js/bootstrap.js',
  'resources/src/js/app.js'
];

var _fonts = [
  'node_modules/simple-line-icons/fonts/Simple-Line-Icons.eot',
  'node_modules/simple-line-icons/fonts/Simple-Line-Icons.svg',
  'node_modules/simple-line-icons/fonts/Simple-Line-Icons.ttf',
  'node_modules/simple-line-icons/fonts/Simple-Line-Icons.woff',
  'node_modules/simple-line-icons/fonts/Simple-Line-Icons.woff2'
];

var _browser = {
  proxy: 'https://nunu.dev/profile',
  files: ['templates/*.twig']
};

var _css_browser_support = ['> 1%', 'last 2 versions', 'IE >= 10'];

//tasks

gulp.task('browser-sync', function() {
  browserSync.init(_browser);
});

gulp.task('clean-css', function() {
  return gulp.src('resources/dist/css', {read: false})
    .pipe(clean());
});

gulp.task('clean-js', function() {
  return gulp.src('resources/dist/js', {read: false})
    .pipe(clean());
});

gulp.task('sass', ['clean-css'], function () {
  var pcPlug = {
    autoprefixer: autoprefixer,
    mqpacker: mqpacker,
  };
  var pcProcess = [
    pcPlug.autoprefixer({
      browsers: _css_browser_support
    }),
    pcPlug.mqpacker()
  ];

  return gulp.src(_sass)
    .pipe(hash())
    .pipe(sourcemaps.init())
    .pipe(sass())
    .on('error', function (err) {
      console.log(err);
      this.emit('end');
    })
    .pipe(postcss(pcProcess))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('resources/dist/css'))
    .pipe(hash.manifest('resources/dist/css/manifest.json', {
      deleteOld: true,
      sourceDir: __dirname + '/resources/dist/css'
    }))
    .pipe(gulp.dest('.'));
});

gulp.task('js', ['clean-js'], function() {
  return gulp.src(_js)
    .pipe(hash())
    .pipe(sourcemaps.init())
  	.pipe(babel())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('resources/dist/js'))
    .pipe(hash.manifest('resources/dist/js/manifest.json', {
      deleteOld: true,
      sourceDir: __dirname + '/resources/dist/js'
    }))
    .pipe(gulp.dest('.'));
});

gulp.task('fonts', function() {
  return gulp.src(_fonts)
    .pipe(gulp.dest('resources/dist/fonts'));
});
