<?php

use BrandRehab\PimPush\Unit\Test;

class MemberControllerTest extends Test
{
  public function testRead()
  {
    $this->get('member/1');

    $this->assertEquals(200, $this->getStatusCode());
    $this->assertEquals(1, $this->getPayload()->id);
    $this->assertEquals('Wayne Ashley', $this->getPayload()->name);
  }
}
