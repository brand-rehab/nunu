<?php

use BrandRehab\PimPush\Unit\Test;

class ProfileControllerTest extends Test
{
  public function testRead()
  {
    $this->get('profile');

    $this->assertEquals(200, $this->getStatusCode());
    $this->assertEquals(1, $this->getPayload()->id);
    $this->assertEquals('Wayne Ashley', $this->getPayload()->name);
  }
}
